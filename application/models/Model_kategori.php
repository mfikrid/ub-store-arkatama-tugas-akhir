<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_kategori extends CI_Model
{
	public function elektronik()
	{
		return $this->db->get_where('product', array('kategori' => 'Brodo'));
	}

	public function shirt()
	{
		return $this->db->get_where('product', array('kategori' => 'Ventela'));
	}

	public function shoes()
	{
		return $this->db->get_where('product', array('kategori' => 'Compass'));
	}

	public function jacket()
	{
		return $this->db->get_where('product', array('kategori' => 'Aerostreet'));
	}

	public function kids()
	{
		return $this->db->get_where('product', array('kategori' => 'Geoff Max'));
	}

	public function fashion()
	{
		return $this->db->get_where('product', array('kategori' => 'NAH Project'));
	}
}
