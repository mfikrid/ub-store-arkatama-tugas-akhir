<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Categories extends CI_Controller
{

	public function elektronik()
	{
		$data['title'] = 'Brodo Categories';
		$data['elektronik'] = $this->model_kategori->elektronik()->result();
		$this->load->view('layout/user/header', $data);
		$this->load->view('electronic', $data);
		$this->load->view('layout/user/footer');
	}

	public function shirt()
	{
		$data['title'] = 'Ventela Categories';
		$data['shirt'] = $this->model_kategori->shirt()->result();
		$this->load->view('layout/user/header', $data);
		$this->load->view('shirt', $data);
		$this->load->view('layout/user/footer');
	}

	public function shoes()
	{
		$data['title'] = 'Compass Categories';
		$data['shoes'] = $this->model_kategori->shoes()->result();
		$this->load->view('layout/user/header', $data);
		$this->load->view('shoes', $data);
		$this->load->view('layout/user/footer');
	}

	public function jacket()
	{
		$data['title'] = 'Aerostreet Categories';
		$data['jacket'] = $this->model_kategori->jacket()->result();
		$this->load->view('layout/user/header', $data);
		$this->load->view('jacket', $data);
		$this->load->view('layout/user/footer');
	}

	public function kids()
	{
		$data['title'] = 'Geoff Max Categories';
		$data['kids'] = $this->model_kategori->kids()->result();
		$this->load->view('layout/user/header', $data);
		$this->load->view('kids', $data);
		$this->load->view('layout/user/footer');
	}

	public function fashion()
	{
		$data['title'] = 'NAH Project Categories';
		$data['fashion'] = $this->model_kategori->fashion()->result();
		$this->load->view('layout/user/header', $data);
		$this->load->view('fashion', $data);
		$this->load->view('layout/user/footer');
	}
}
