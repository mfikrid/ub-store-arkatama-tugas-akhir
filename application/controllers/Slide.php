<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Slide extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('level') != '2') {
            redirect('welcome');
        }
    }

    public function index()
    {
        $data['title'] = 'Slide';
        $data['product'] = $this->model_pembayaran->get('product')->result();
        $this->load->view('slide', $data);
    }
}
